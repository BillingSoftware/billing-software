import { reduce as _reduce } from 'lodash';

// Utils
import manageBillsUtils from '../modules/ManageBills/manageBillsUtils';

export default {
    companyDetails: {
        name: '',
        address1: '',
        address2: '',
        contactNo: '',
        email: '',
        gstNo: '',
        termsAndConditions: '',
    },
    customersList: [],
    productsList: [],
    billsList: [],
    taxDetails: {
        SGST: 9,
        CGST: 9,
        IGST: 18,
        SGSTEnabled: true,
        CGSTEnabled: true,
        IGSTEnabled: false,
    },
    manageBillsColumnConfig: _reduce(manageBillsUtils.getTableColumns(),(accumulator,column) => {
        if(!column.isRequired) {
            accumulator[column.dataIndex]=true; 
        }
        return accumulator;
    },{}),
};