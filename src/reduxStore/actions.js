import actionTypes from './actionTypes';

// Company

const updateCompanyDetails = (updatedDetails) => {
    return {
        type: actionTypes.UPDATE_COMPANY_DETAILS,
        payload: updatedDetails,
    };
};

// Tax Details

const updateTaxDetails = (updatedDetails) => {
    return {
        type: actionTypes.UPDATE_TAX_DETAILS,
        payload: updatedDetails,
    };
};

// Manage Bills Column Config

const updateManageBillsColumnConfig = (updatedDetails) => {
    return {
        type: actionTypes.UPDATE_MANAGE_BILLS_COULMN_CONFIG,
        payload: updatedDetails,
    }
};

// Customers

const createCustomer = (customerDetails) => {
    return {
        type: actionTypes.CREATE_CUSTOMER,
        payload: customerDetails,
    }
};

const editCustomer = (customerDetails) => {
    return {
        type: actionTypes.EDIT_CUSTOMER,
        payload: customerDetails,
    }
};

const deleteCustomer = (customerDetails) => {
    return {
        type: actionTypes.DELETE_CUSTOMER,
        payload: customerDetails,
    }
};

// Products

const createProduct = (productDetails) => {
    return {
        type: actionTypes.CREATE_PRODUCT,
        payload: productDetails,
    }
};

const editProduct = (productDetails) => {
    return {
        type: actionTypes.EDIT_PRODUCT,
        payload: productDetails,
    }
};

const deleteProduct = (productDetails) => {
    return {
        type: actionTypes.DELETE_PRODUCT,
        payload: productDetails,
    }
};

// Bills

const saveBill = (billDetails) => {
    return {
        type: actionTypes.SAVE_BILL,
        payload: billDetails,
    }
};

const deleteBill = (index) => {
    return {
        type: actionTypes.DELETE_BILL,
        payload: index,
    };
};

// Data

const saveData = (data) => {
    return {
        type: actionTypes.SAVE_DATA,
        payload: data,
    }
};

export default {
    updateCompanyDetails,
    updateTaxDetails,
    updateManageBillsColumnConfig,
    createCustomer,
    editCustomer,
    deleteCustomer,
    createProduct,
    editProduct,
    deleteProduct,
    saveBill,
    deleteBill,
    saveData,
}