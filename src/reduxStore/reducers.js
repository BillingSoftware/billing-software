import actionTypes from './actionTypes';
import initialState from './initialState';

const reducers = (state = initialState,action) => {
    const { type, payload } = action;
    switch(type) {
        case actionTypes.UPDATE_COMPANY_DETAILS: return {
            ...state,
            companyDetails: {
                ...state.companyDetails,
                ...payload,
            }
        };
        case actionTypes.UPDATE_TAX_DETAILS: return {
            ...state,
            taxDetails: {
                ...state.taxDetails,
                ...payload,
            }
        };
        case actionTypes.UPDATE_MANAGE_BILLS_COULMN_CONFIG: return {
            ...state,
            manageBillsColumnConfig: {
                ...state.manageBillsColumnConfig,
                ...payload,
            }
        };

        case actionTypes.CREATE_CUSTOMER: return {
            ...state,
            customersList: [payload, ...state.customersList],
        };
        case actionTypes.EDIT_CUSTOMER: return {
            ...state,
            customersList: state.customersList.map(customer => {
                if(customer.name===payload.name) {
                    return payload;
                }
                return customer;
            })
        };
        case actionTypes.DELETE_CUSTOMER: return {
            ...state,
            customersList: state.customersList.filter(customer => {
                return customer.name!==payload.name
            })
        };

        case actionTypes.CREATE_PRODUCT: return {
            ...state,
            productsList: [payload, ...state.productsList],
        };
        case actionTypes.EDIT_PRODUCT: return {
            ...state,
            productsList: state.productsList.map(product => {
                if(product.name===payload.name) {
                    return payload;
                }
                return product;
            })
        };
        case actionTypes.DELETE_PRODUCT: return {
            ...state,
            productsList: state.productsList.filter(product => {
                return product.name!==payload.name;
            })
        };

        case actionTypes.SAVE_BILL: return {
            ...state,
            billsList: [...state.billsList,payload],
        };
        case actionTypes.DELETE_BILL: return {
            ...state,
            billsList: state.billsList.filter((bill,index) => index!==payload),
        };

        case actionTypes.SAVE_DATA: return {
            ...state,
            ...payload,
        };

        default: return state;
    }
};

export default reducers;