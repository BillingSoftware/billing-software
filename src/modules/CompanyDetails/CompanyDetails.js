import React, { useCallback } from 'react';
import styles from './CompanyDetails.module.css';

// Actions
import { connect } from "react-redux";
import actions from '../../reduxStore/actions'; 

// Components
import { Form, Typography } from 'antd';
import Input from '../../components/Input/Input';
import TextArea from '../../components/TextArea/TextArea';

const CompanyDetails = props => {

  const { companyDetails, updateCompanyDetails } = props;

  const onCompanyDetailsChange = useCallback((changeItem) => {
    updateCompanyDetails(changeItem);
  },[updateCompanyDetails]);

  return (
    <div className={styles.section_container}>
      <Typography.Title>Company Details</Typography.Title>
      <Form
        labelCol={{ span: 6 }}
        wrapperCol={{span: 16}}
      >
        <Form.Item label="Name">
          <Input
            datakey="name"
            placeholder="Please Enter Name"
            value={companyDetails}
            onChange={onCompanyDetailsChange}/>
        </Form.Item>
        <Form.Item label="Address">
          <Input
            style={{ marginBottom: "24px" }}
            datakey="address1"
            placeholder="Please Enter Address Line 1"
            value={companyDetails}
            onChange={onCompanyDetailsChange}/>
          <Input
            datakey="address2"
            placeholder="Please Enter Address Line 2"
            value={companyDetails}
            onChange={onCompanyDetailsChange}/>
        </Form.Item>
        <Form.Item label="Contact Number">
          <Input
            datakey="contactNo"
            placeholder="Please Enter Contact Number"
            value={companyDetails}
            onChange={onCompanyDetailsChange}/>
        </Form.Item>
        <Form.Item label="Email">
          <Input
            datakey="email"
            placeholder="Please Enter Email"
            value={companyDetails}
            onChange={onCompanyDetailsChange}/>
        </Form.Item>
        <Form.Item label="GST Number">
          <Input
            datakey="gstNo"
            placeholder="Please Enter GST Number"
            value={companyDetails}
            onChange={onCompanyDetailsChange}/>
        </Form.Item>
        <Form.Item label="Terms And Conditions">
          <TextArea
            rows={6}
            datakey="termsAndConditions"
            placeholder="Please Enter Terms And Conditions"
            value={companyDetails}
            onChange={onCompanyDetailsChange}/>
        </Form.Item>
      </Form>
    </div>
  );
};

const mapStateToProps = state => {
  return {
    companyDetails: state.companyDetails,
  }
};

const mapDispatchToProps = dispatch => {
  return {
    updateCompanyDetails: (updatedDetails) => dispatch(actions.updateCompanyDetails(updatedDetails)),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(React.memo(CompanyDetails));
