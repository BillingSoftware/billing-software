import React, { useCallback } from 'react';
import styles from './Settings.module.css';
import actions from '../../reduxStore/actions';

// Actions
import { connect } from "react-redux";

// Components
import { Typography, Button, Divider, Form, Row, Col, Modal } from 'antd';
import Checkbox from '../../components/Checkbox/Checkbox';
import InputNumber from '../../components/InputNumber/InputNumber';

// Utils
import manageBillsUtils from '../ManageBills/manageBillsUtils';

const electron = window.require("electron");
const { dialog } = electron.remote;

const fs = window.require("fs");

const Settings = props => {
  const { allDetails, updateTaxDetails, updateManageBillsColumnConfig, saveData } = props;
  const { taxDetails, manageBillsColumnConfig } = allDetails;

  const backupData = useCallback(() => {
    dialog.showSaveDialog({ defaultPath: 'billing_software_backup_data.json' }).then((response) => {
      const downloadFilePath = response?.filePath;
      if(downloadFilePath) {
        fs.writeFile(downloadFilePath,JSON.stringify(allDetails),'utf8',(err) => {
          if(err) {
            Modal.success({ title: 'Data backup failed !!!' });
            return;
          }
          Modal.success({ title: 'Data backup successfully !!!' });
        });
      };
    });
  },[allDetails]);

  const restoreData = useCallback(() => {
    dialog.showOpenDialog({
      properties: ['openFile']
   }).then(data => {
    const restoreFilePath = data?.filePaths?.[0];
    if(restoreFilePath) {
      fs.readFile(restoreFilePath,'utf8',(err,data) => {
        if(err) {
          Modal.success({ title: 'Failed to restore data !!!' });
          return;
        }
        saveData(JSON.parse(data));
        Modal.success({ title: 'Data restored successfully !!!' });
      });
    };
  });
  },[saveData]);

  const onTaxDetailsChange = useCallback((changeItem) => {
    updateTaxDetails(changeItem);
  },[updateTaxDetails]);

  const onManageBillsColumnConfigChange = useCallback((changeItem) => {
    updateManageBillsColumnConfig(changeItem);
  },[updateManageBillsColumnConfig]);

  return (
    <div className={styles.section_container}>
      <Typography.Title>Settings</Typography.Title>

      <Divider orientation="left">Tax Details</Divider>

      <Form>
        <Row>
          <Col span={12}>
            <Form.Item 
              label="SGST Rate"
              labelCol={{span: 6}}
              wrapperCol={{span: 6}}>
              <InputNumber 
                datakey='SGST'
                precision={2}
                value={taxDetails}
                onChange={onTaxDetailsChange}/>
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item 
              label="Apply SGST"
              labelCol={{span: 6}}
              wrapperCol={{span: 1}}>
              <Checkbox 
                datakey='SGSTEnabled'
                value={taxDetails}
                onChange={onTaxDetailsChange}/>
            </Form.Item>
          </Col>
        </Row>

        <Row>
          <Col span={12}>
            <Form.Item 
              label="CGST Rate"
              labelCol={{span: 6}}
              wrapperCol={{span: 6}}>
              <InputNumber 
                datakey='CGST'
                precision={2}
                value={taxDetails}
                onChange={onTaxDetailsChange}/>
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item 
              label="Apply CGST"
              labelCol={{span: 6}}
              wrapperCol={{span: 1}}>
              <Checkbox 
                datakey='CGSTEnabled'
                value={taxDetails}
                onChange={onTaxDetailsChange}/>
            </Form.Item>
          </Col>
        </Row>

        <Row>
          <Col span={12}>
            <Form.Item 
              label="IGST Rate"
              labelCol={{span: 6}}
              wrapperCol={{span: 6}}>
              <InputNumber 
                datakey='IGST'
                precision={2}
                value={taxDetails}
                onChange={onTaxDetailsChange}/>
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item 
              label="Apply IGST"
              labelCol={{span: 6}}
              wrapperCol={{span: 1}}>
              <Checkbox 
                datakey='IGSTEnabled'
                value={taxDetails}
                onChange={onTaxDetailsChange}/>
            </Form.Item>
          </Col>
        </Row>
      </Form>

      <Divider orientation="left">Manage Bills Column Configuration</Divider>
      {
        manageBillsUtils.getTableColumns().map(column => {
          return (
            <Row justify='start'>
              <Col>
                {column.isRequired ? (
                  null
                ) : (
                  <Checkbox 
                    datakey={column.dataIndex}
                    value={manageBillsColumnConfig}
                    onChange={onManageBillsColumnConfigChange}>
                    {column.title}
                  </Checkbox>
                )}
              </Col>
            </Row>
          );
        })
      }

      <Divider orientation="left">Data Management</Divider>

      <Button
        type="dashed"
        onClick={backupData}
        style={{width: "100%"}}>
        Backup Data
      </Button>

      <Button
        type="dashed"
        onClick={restoreData}
        style={{marginTop:'24px', marginBottom:'24px', width: "100%"}}>
        Restore Data
      </Button>

    </div>
  );
};

const mapStateToProps = state => {
  return {
    allDetails: state,
  }
};

const mapDispatchToProps = dispatch => {
  return {
    updateTaxDetails: (updatedDetails) => dispatch(actions.updateTaxDetails(updatedDetails)),
    updateManageBillsColumnConfig: (updatedDetails) => dispatch(actions.updateManageBillsColumnConfig(updatedDetails)),
    saveData: (data) => dispatch(actions.saveData(data)),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(React.memo(Settings));
