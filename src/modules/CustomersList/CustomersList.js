import React, { useState, useCallback, useMemo } from 'react';
import styles from './CustomersList.module.css';
import { find as _find } from 'lodash';

// Actions
import { connect } from "react-redux";
import actions from '../../reduxStore/actions'; 

// Components
import { Typography, Table, Button, Space, Modal } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import CreateCustomerModal from './CreateCustomerModal';

const getTableColumns = (onEditExistingCustomerClick,deleteCustomer) => {
  return [
    {
      title: 'Name',
      dataIndex: 'name',
    },
    {
      title: 'Contact Number',
      dataIndex: 'contactNo',
    },
    {
      title: 'Email',
      dataIndex: 'email',
    },
    {
      title: 'GST Number',
      dataIndex: 'gstNo',
    },
    {
      title: 'Actions',
      dataIndex: 'actions',
      render: (text,record) => {
        return (
          <Space>
            <a onClick={() => onEditExistingCustomerClick(record)}>Edit</a>
            <a onClick={() => deleteCustomer(record)}>Delete</a>
          </Space>
        );
      },
    }
  ];
};

const tableExpandableProperties = {
  expandedRowRender: record => <p>{`Address: ${record.address1}, ${record.address2}`}</p>
}; 

const CustomersList = props => {
  const { 
    customersList, 
    createCustomer, 
    editCustomer, 
    deleteCustomer, 
  } = props;

  const [showCreateCustomerModal, setShowCreateCustomerModal] = useState({
    visible: false,
    mode: undefined,
    value: undefined,
  });

  // Open and close modal

  const openCreateCustomerModal = useCallback((mode,value) => {
    setShowCreateCustomerModal({
      visible: true,
      mode,
      value,
    });
  },[setShowCreateCustomerModal]);

  const closeCreateCustomerModal = useCallback(() => {
    setShowCreateCustomerModal({
      visible: false,
      mode: undefined,
      value: undefined,
    });
  },[setShowCreateCustomerModal]);

  // Create new customer

  const onCreateNewCustomerClick = useCallback(() => {
    openCreateCustomerModal('CREATE');
  },[openCreateCustomerModal]);

  const createNewCustomer = useCallback((customerDetails) => {
    const existingCustomer = _find(customersList,['name',customerDetails.name]);
    if(existingCustomer) {
      Modal.error({ title: 'Customer with same name already exist, please use different name.' });
      return;
    }
    
    if(createCustomer) {
      createCustomer(customerDetails);
    }
    closeCreateCustomerModal();
  },[customersList,createCustomer,closeCreateCustomerModal]);

  // Edit existing customer

  const onEditExistingCustomerClick = useCallback((customerDetails) => {
    openCreateCustomerModal('EDIT',customerDetails);
  },[openCreateCustomerModal]);

  const editExistingCustomer = useCallback((customerDetails) => {
    if(editCustomer) {
      editCustomer(customerDetails);
    }
    closeCreateCustomerModal();
  },[editCustomer,closeCreateCustomerModal]);

  const tableColumns = useMemo(() => getTableColumns(onEditExistingCustomerClick,deleteCustomer),[onEditExistingCustomerClick,deleteCustomer]);

  return (
    <div className={styles.section_container}>
      <Typography.Title>Customers List</Typography.Title>
      <Button
        type="dashed"
        onClick={onCreateNewCustomerClick}
        style={{marginBottom: "24px", width: "100%"}}
      > 
        <PlusOutlined /> Create New Customer
      </Button>
      <Table 
        rowKey="name"
        columns={tableColumns} 
        dataSource={customersList} 
        bordered={true} 
        expandable={tableExpandableProperties}/>
      {showCreateCustomerModal.visible && (
        <CreateCustomerModal
          {...showCreateCustomerModal}
          title="Enter Details Of New Customer"
          centered={true}
          onOk={showCreateCustomerModal.mode==='CREATE' ? createNewCustomer : editExistingCustomer}
          onCancel={closeCreateCustomerModal} />
      )}
    </div>
  );
};

const mapStateToProps = state => {
  return {
    customersList: state.customersList
  }
};

const mapDispatchToProps = dispatch => {
  return {
    createCustomer: (customerDetails) => dispatch(actions.createCustomer(customerDetails)),
    editCustomer: (customerDetails) => dispatch(actions.editCustomer(customerDetails)),
    deleteCustomer: (customerDetails) => dispatch(actions.deleteCustomer(customerDetails)),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(React.memo(CustomersList));

