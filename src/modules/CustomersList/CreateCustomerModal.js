import React, { useState, useCallback } from 'react';

// Components
import { Form, Modal } from 'antd';
import Input from '../../components/Input/Input';

const CreateCustomerModal = (props) => {
    const { value, mode, onOk } = props;
    
    const [customerDetails, setCustomerDetails] = useState(value || {
        name: "",
        address1: "",
        address2: "",
        contactNo: "",
        email: "",
        gstNo: "",
    });

    const onCustomerDetailsChange = useCallback((changeItem) => {
        setCustomerDetails({
            ...customerDetails,
            ...changeItem,
        })
    },[customerDetails,setCustomerDetails]);

    const onCreateCustomer = useCallback(() => {
        if(!onOk) {
            return;
        }
        onOk(customerDetails);
    },[customerDetails,onOk]);

    return (
        <Modal 
            {...props}
            onOk={onCreateCustomer}>
            <Form
                labelCol={{ span: 6 }}
                wrapperCol={{span: 16}}>
                <Form.Item label="Name">
                    <Input
                        datakey="name"
                        placeholder="Please Enter Name"
                        disabled={mode==='EDIT'}
                        value={customerDetails}
                        onChange={onCustomerDetailsChange}/>
                </Form.Item>
                <Form.Item label="Address">
                    <Input
                        style={{ marginBottom: "24px" }}
                        datakey="address1"
                        placeholder="Please Enter Address Line 1"
                        value={customerDetails}
                        onChange={onCustomerDetailsChange}/>
                    <Input
                        datakey="address2"
                        placeholder="Please Enter Address Line 2"
                        value={customerDetails}
                        onChange={onCustomerDetailsChange}/>
                </Form.Item>
                <Form.Item label="Contact Number">
                    <Input
                        datakey="contactNo"
                        placeholder="Please Enter Contact Number"
                        value={customerDetails}
                        onChange={onCustomerDetailsChange}/>
                </Form.Item>
                <Form.Item label="Email">
                    <Input
                        datakey="email"
                        placeholder="Please Enter Email"
                        value={customerDetails}
                        onChange={onCustomerDetailsChange}/>
                </Form.Item>
                <Form.Item label="GST Number">
                    <Input
                        datakey="gstNo"
                        placeholder="Please Enter GST Number"
                        value={customerDetails}
                        onChange={onCustomerDetailsChange}/>
                </Form.Item>
            </Form>
        </Modal>
    );
};

export default React.memo(CreateCustomerModal);