import React, { useState, useCallback, useEffect } from 'react';
import './App.css';
import { map as _map } from 'lodash';

// Redux Store
import { Provider } from 'react-redux';
import store from '../../reduxStore/store';

// Components
import { Layout, Menu, Modal } from "antd";
import { Spin } from 'antd'; 

// Utils
import appUtils from './appUtils';
import actions from '../../reduxStore/actions';

const path = window.require('path');
const fs = window.require('fs');
const electron = window.require("electron");
const { remote } = electron;
const { app } = remote;

const sidebarSections = appUtils.sections;
const sidebarSectionToComponentMap = appUtils.sectionToComponentMap;

function App() {
  const [dataSaved, setDataSaved] = useState(false);
  const [loadingData, setLoadingData] = useState(true);
  const [activeSection, setActiveSection] = useState(sidebarSections.COMPANY_DETAILS.value);
  const Component = sidebarSectionToComponentMap[activeSection];

  const changeActiveSection = useCallback((changeItem) => {
    setActiveSection(changeItem.key);
  },[setActiveSection]);

  useEffect(() => {
    fs.readFile(path.join(app.getPath('userData'),'savedData.json'),'utf8',(err,data) => {
      if(data) {
        store.dispatch(actions.saveData(JSON.parse(data)));
      }
      setLoadingData(false);
    });
  },[]);

  useEffect(() => {
    window.onbeforeunload = () => {
      if(dataSaved===true) {
        return;
      }

      const data = store.getState(); 
      fs.writeFile(path.join(app.getPath('userData'),'savedData.json'),JSON.stringify(data),'utf8',(err) => {
        if(err) {
          Modal.error({ title: "Failed to save data. Please try again." });
          return;
        }
        Modal.success({ title: "Data saved successfully.", onOk: () => window.close() });
        setDataSaved(true);
      });
      return 'something to avoid closing app';
    };
  },[dataSaved]);

  return (
    <Provider store={store}>
      <Layout className="app">
        { loadingData ? (
            <Spin size="large" className='spinner'></Spin>
          ) : (
            <>
              <Layout.Sider className="section-list-container">
                <Menu
                  theme="dark"
                  mode="inline"
                  onSelect={changeActiveSection}
                  defaultSelectedKeys={[activeSection]}
                  style={{ height: "100%", paddingTop: "24px" }}>
                  {_map(sidebarSections,section => (<Menu.Item key={section.value}>{section.label}</Menu.Item>))}
                </Menu>
              </Layout.Sider>
              <Layout.Content className="section-container">
                <Component />
              </Layout.Content>
            </>
          )
        }
      </Layout>
      <div id="bill_sample" style={{ opacity: 0, position: "absolute", left: "-999px", width: '780px', minWidth: '780px', maxWidth: '780px' }}>
      </div>
    </Provider>
  );
}

export default React.memo(App);
