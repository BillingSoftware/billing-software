// Components
import CompanyDetails from '../CompanyDetails/CompanyDetails';
import CustomersList from '../CustomersList/CustomersList';
import ProductsList from '../ProductsList/ProductsList';
import CreateBill from '../CreateBill/CreateBill';
import ManageBills from '../ManageBills/ManageBills';
import Settings from '../Settings/Settings';

const sections = {
    COMPANY_DETAILS : {
        value: 'COMPANY_DETAILS',
        label: 'Company Details',
    },
    CUSTOMERS_LIST: {
        value: 'CUSTOMERS_LIST',
        label: 'Customers List',
    },
    PRODUCTS_LIST: {
        value: 'PRODUCTS_LIST',
        label: 'Products List',
    },
    CREATE_BILL: {
        value: 'CREATE_BILL',
        label: 'Create Bill',
    },
    MANAGE_BILLS: {
        value: 'MANAGE_BILLS',
        label: 'Manage Bills',
    },
    SETTINGS: {
        value: 'SETTINGS',
        label: 'Settings',
    },
};

const sectionToComponentMap = {
    [sections.COMPANY_DETAILS.value]: CompanyDetails,
    [sections.CUSTOMERS_LIST.value]: CustomersList,
    [sections.PRODUCTS_LIST.value]: ProductsList,
    [sections.CREATE_BILL.value]: CreateBill,
    [sections.MANAGE_BILLS.value]: ManageBills,
    [sections.SETTINGS.value]: Settings,
};

export default {
    sections,
    sectionToComponentMap
};