import React, { useState, useCallback, useMemo } from 'react';
import styles from './CreateBill.module.css';
import { reduce as _reduce, round as _round } from 'lodash';

// Actions
import { connect } from "react-redux";
import actions from '../../reduxStore/actions'; 

// Components
import { Typography, Form, Row, Col, Button, Modal } from 'antd';
import Input from '../../components/Input/Input';
import Select from '../../components/Select/Select';
import DatePicker from '../../components/DatePicker/DatePicker';
import InputNumber from '../../components/InputNumber/InputNumber';
import CreateBillTable from './CreateBillTable';

// Utils
import createBillUtils from './createBillUtils';
import downloadBillUtils from './downloadBillUtils';

const PRECISION = 2;
const inputNumberStyle = { width: '100%' };

const CreateBill = props => {
  const { companyDetails, customersList, productsList, taxDetails, saveBill } = props;

  const [billDetails, setBillDetails] = useState({
    billNumber: '',
    billNumberDate: '',
    challanNumber: '',
    challanNumberDate: '',
    orderNumber: '',
    orderNumberDate: '',
    customer: '',
    selledProducts: [...createBillUtils.DEFAULT_DATA_SOURCE],
  });

  const onBillDetailsChange = useCallback((changeItem) => {
    setBillDetails({
      ...billDetails,
      ...changeItem,
    });
  },[billDetails,setBillDetails]);

  const priceDetails = useMemo(() => {
    const { selledProducts } = billDetails;
    const totalAmountWithoutTax = _round(_reduce(selledProducts,(accumulator,selledProduct) => {
      return accumulator+Number(selledProduct.amount || 0); 
    },0.00),PRECISION);
    
    let SGST = 0.00;
    if(taxDetails.SGSTEnabled) {
      SGST = _round((totalAmountWithoutTax*(taxDetails.SGST))/100.00,PRECISION)
    };
    
    let CGST = 0.00;
    if(taxDetails.CGSTEnabled) {
      CGST = _round((totalAmountWithoutTax*(taxDetails.CGST))/100.00,PRECISION);
    };

    let IGST = 0.00;
    if(taxDetails.IGSTEnabled) {
      IGST = _round((totalAmountWithoutTax*(taxDetails.IGST))/100.00,PRECISION);
    };

    const totalAmountWithTax = _round(totalAmountWithoutTax+SGST+CGST+IGST,0);
    return {
      totalAmountWithoutTax,
      SGST,CGST,IGST,
      totalAmountWithTax,
    };
  },[billDetails,taxDetails])

  const onSaveBillClick = useCallback(() => {
    saveBill(createBillUtils.getBillDetails(billDetails,companyDetails,customersList,priceDetails,taxDetails));
    Modal.success({ title: 'Bill saved successfully.' })
  },[billDetails,companyDetails,customersList,priceDetails,taxDetails,saveBill]);

  const onDownloadBillClick = useCallback(() => {
    const details = createBillUtils.getBillDetails(billDetails,companyDetails,customersList,priceDetails,taxDetails);
    downloadBillUtils.downloadBill(details);
  },[billDetails,companyDetails,customersList,priceDetails,taxDetails]);

  const onDownloadAndSaveBillClick = useCallback(() => {
    onSaveBillClick();
    onDownloadBillClick();
  },[onSaveBillClick,onDownloadBillClick]);

  return (
    <div className={styles.section_container}>
      <Typography.Title>Create Bill</Typography.Title>
      <Form>

        {/* Bill Number */}
        <Row>
          <Col span={12}>
            <Form.Item 
              label="Bill Number" 
              labelCol={{span: 8}}
              wrapperCol={{span: 8}}>
              <Input 
                datakey="billNumber" 
                value={billDetails}
                onChange={onBillDetailsChange}/>
            </Form.Item>  
          </Col>
          <Col span={12}>
            <Form.Item 
              label="Date" 
              labelCol={{span: 4}}
              wrapperCol={{span: 8}}>
              <DatePicker 
                format="DD/MM/YYYY"
                datakey="billNumberDate"
                onChange={onBillDetailsChange}/>
            </Form.Item>
          </Col>
        </Row>

        {/* Challan Number */}
        <Row>
          <Col span={12}>
            <Form.Item 
              label="Challan Number" 
              labelCol={{span: 8}}
              wrapperCol={{span: 8}}>
              <Input 
                datakey="challanNumber" 
                value={billDetails}
                onChange={onBillDetailsChange}/>
            </Form.Item>  
          </Col>
          <Col span={12}>
            <Form.Item 
              label="Date" 
              labelCol={{span: 4}}
              wrapperCol={{span: 8}}>
              <DatePicker 
                format="DD/MM/YYYY"
                datakey="challanNumberDate"
                onChange={onBillDetailsChange}/>
            </Form.Item>
          </Col>
        </Row>

        {/* Order Number */}
        <Row>
          <Col span={12}>
            <Form.Item 
              label="Order Number" 
              labelCol={{span: 8}}
              wrapperCol={{span: 8}}>
              <Input 
                datakey="orderNumber" 
                value={billDetails}
                onChange={onBillDetailsChange}/>
            </Form.Item>  
          </Col>
          <Col span={12}>
            <Form.Item 
              label="Date" 
              labelCol={{span: 4}}
              wrapperCol={{span: 8}}>
              <DatePicker 
                format="DD/MM/YYYY"
                datakey="orderNumberDate"
                onChange={onBillDetailsChange}/>
            </Form.Item>
          </Col>
        </Row>

        {/* Customer */}
        <Row>
          <Col span={24}>
            <Form.Item
              label="Customer"
              labelCol={{span: 4}}
              wrapperCol={{span: 14}}>
              <Select 
                showSearch
                datakey="customer"
                labelkey="name"
                valuekey="name"
                value={billDetails}
                options={customersList}
                onChange={onBillDetailsChange}
                filterOption={(input, option) => option.label.toLowerCase().includes(input.toLowerCase())}/>
            </Form.Item>
          </Col>
        </Row>

        {/* Selled Products */}
        <Row>
          <Col span={24}>
            <CreateBillTable 
              datakey="selledProducts"
              value={billDetails}
              onChange={onBillDetailsChange}
              productsList={productsList}/>
          </Col>
        </Row>

        {/* Price Details */}
        <Row>
          <Col span={24}>
            <Form.Item 
              label="Total (Without Tax)"  
              labelCol={{span: 4}}
              wrapperCol={{span: 4}}>
              <InputNumber 
                style={{ width:"100%" }}
                readOnly={true}
                datakey="totalAmountWithoutTax"
                value={priceDetails}/>
            </Form.Item>
          </Col>
        </Row>

        <Row>
          <Col span={8}>
            <Form.Item 
              label="SGST 9%"  
              labelCol={{span: 12}}
              wrapperCol={{span: 12}}>
              <InputNumber 
                style={inputNumberStyle}
                readOnly={true}
                datakey="SGST"
                value={priceDetails}/>
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item 
              label="CGST 9%"  
              labelCol={{span: 12}}
              wrapperCol={{span: 12}}>
              <InputNumber 
                style={inputNumberStyle}
                readOnly={true}
                datakey="CGST"
                value={priceDetails}/>
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item 
              label="IGST 18%"  
              labelCol={{span: 12}}
              wrapperCol={{span: 12}}>
              <InputNumber 
                style={inputNumberStyle}
                readOnly={true}
                datakey="IGST"
                value={priceDetails}/>
            </Form.Item>
          </Col>
        </Row>

        <Row>
          <Col span={24}>
            <Form.Item 
              label="Total (With Tax)"  
              labelCol={{span: 4}}
              wrapperCol={{span: 4}}>
              <InputNumber 
                style={inputNumberStyle}
                readOnly={true}
                datakey="totalAmountWithTax"
                value={priceDetails}/>
            </Form.Item>
          </Col>
        </Row>

        {/* Bill Actions */}
        <Row style={{marginBottom:"24px"}}>
          <Col span={8}>
            <Button
              type="primary"
              onClick={onSaveBillClick}>
              Save
            </Button>
          </Col>
          <Col span={8}>
            <Button
              type="primary"
              onClick={onDownloadBillClick}>
              Download
            </Button>
          </Col>
          <Col span={8}>
            <Button
              type="primary"
              onClick={onDownloadAndSaveBillClick}>
              Save And Download
            </Button>
          </Col>
        </Row>

      </Form>
    </div>
  );
};

const mapStateToProps = state => {
  return {
    companyDetails: state.companyDetails,
    customersList: state.customersList,
    productsList: state.productsList,
    taxDetails: state.taxDetails,
  }
};

const mapDispatchToProps = dispatch => {
  return {
    saveBill: (billDetails) => dispatch(actions.saveBill(billDetails)),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(React.memo(CreateBill));

