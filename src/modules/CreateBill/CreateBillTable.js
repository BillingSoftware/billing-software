import React, { useCallback, useMemo } from 'react';
import { round as _round } from 'lodash';

// Components
import { Table, Button, Modal } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import Input from '../../components/Input/Input';
import Select from '../../components/Select/Select';
import InputNumber from '../../components/InputNumber/InputNumber';

// Utils
import createBillUtils from './createBillUtils';

const PRECISION = 2;

const getTableColumns = (productsList,onRowValueChange) => {
    return [
        {
            title: 'Product',
            dataIndex: 'product',
            width: '40%',
            render: (_,record,index) => {
                const onChange = (value,selectedOption) => {
                    onRowValueChange({
                        product: selectedOption?.name || '',
                        code: selectedOption?.code || '',
                        rate: selectedOption?.cost || '',
                    },index);
                };
                return (
                    <Select
                        style={{width: "100%"}}
                        allowClear
                        showSearch
                        datakey="product"
                        labelkey="name"
                        valuekey="name"
                        value={record}
                        options={productsList}
                        onChange={onChange}
                        filterOption={(input, option) => option.label.toLowerCase().includes(input.toLowerCase())}/>
                );
            }
        },
        {
            title: 'Code',
            dataIndex: 'code',
            width: '15%',
            render: (_,record,index) => {
                const onChange = (changeItem) => {
                    onRowValueChange(changeItem,index);
                };
                return (
                    <Input 
                        datakey="code"
                        value={record}
                        onChange={onChange}/>
                );
            }
        },
        {
            title: 'Rate (Rs.)',
            dataIndex: 'rate',
            width: '15%',
            render: (_,record,index) => {
                const onChange = (changeItem) => {
                    onRowValueChange(changeItem,index);
                };
                return (
                    <InputNumber 
                        precision={PRECISION}
                        datakey="rate"
                        value={record}
                        onChange={onChange}/>
                );
            }
        },
        {
            title: 'Quantity',
            dataIndex: 'quantity',
            width: '15%',
            render: (_,record,index) => {
                const onChange = (changeItem) => {
                    onRowValueChange(changeItem,index);
                };
                return (
                    <InputNumber 
                        precision={0}
                        datakey="quantity"
                        value={record}
                        onChange={onChange}/>
                );
            }
        },
        {
            title: 'Amount',
            dataIndex: 'amount',
            width: '15%',
            render: (_,record,index) => {
                const onChange = (changeItem) => {
                    onRowValueChange(changeItem,index);
                };
                return (
                    <InputNumber 
                        precision={PRECISION}
                        datakey="amount"
                        value={record}
                        onChange={onChange}/>
                );
            }
        },
    ];
};

const CreateBillTable = props => {
    const { datakey, onChange, productsList } = props;
    const selledProducts = datakey ? props.value?.[datakey] : props.value;
    
    const onAddNewRowClick = useCallback(() => {
        if(!onChange) {
            return;
        };
        
        const { MAX_ROW_COUNT } = createBillUtils;
        if(selledProducts.length>=MAX_ROW_COUNT) {
            Modal.error({ title: `Maximum ${MAX_ROW_COUNT} rows are allowed.` })
            return;
        }

        const updatedValue = [...selledProducts,createBillUtils.DEFAULT_DATA_ROW];
        if(datakey) {
            onChange({ [datakey]: updatedValue });
        } else {
            onChange(updatedValue);
        }
    },[datakey,selledProducts,onChange]);
    
    const onRowValueChange = useCallback((changeItem,index) => {
        if(!onChange) {
            return;
        };
        const oldValue = selledProducts[index];
        const newValue = {...oldValue,...changeItem};

        if(newValue.rate && newValue.quantity) {
            newValue.amount = _round(newValue.rate * newValue.quantity,2);
        };

        selledProducts[index] = newValue;
        if(datakey) {
            onChange({ [datakey]: [...selledProducts] });
        } else {
            onChange([...selledProducts]);
        }
    },[datakey,selledProducts,onChange]);

    const tableColumns = useMemo(() => getTableColumns(productsList,onRowValueChange),[productsList,onRowValueChange]);

    return (
        <>  
            <Table 
                bordered={true}
                pagination={false}
                columns={tableColumns}
                dataSource={selledProducts}/>

            <Button
                type="dashed"
                onClick={onAddNewRowClick}
                style={{marginTop: "24px", marginBottom: "24px", width: "100%"}}> 
                <PlusOutlined /> Add New Row
            </Button>
        </>
    );
};

export default React.memo(CreateBillTable);