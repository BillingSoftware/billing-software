import { 
  times as _times,
  constant as _constant,
  every as _every,
  isEmpty as _isEmpty,
  isNumber as _isNumber,
  find as _find,
  pick as _pick,
} from 'lodash';

const DEFAULT_ROW_COUNT = 5;
const MAX_ROW_COUNT = 20;
const DEFAULT_DATA_ROW = { product: '', code: '', rate: '', quantity: '', amount: '', };
const DEFAULT_DATA_SOURCE = _times(DEFAULT_ROW_COUNT,_constant(DEFAULT_DATA_ROW));

const isEmptyObject = (obj = {}) => _every(obj,(val) => (_isEmpty(val) && !_isNumber(val)));

const getBillDetails = (billDetails,companyDetails,customersList,priceDetails,taxDetails) => {
    const customerDetails = _find(customersList,['name',billDetails.customer]) || {};
    const expectedKeys = [
      'billNumber',
      'billNumberDate',
      'challanNumber',
      'challanNumberDate',
      'orderNumber',
      'orderNumberDate',
      'selledProducts',
    ];
    return {
      ..._pick(billDetails,expectedKeys),
      selledProducts: billDetails.selledProducts.filter(column => !isEmptyObject(column)),
      companyDetails,
      customerDetails,
      priceDetails,
      taxDetails,
    };
};
  
export default {
    DEFAULT_ROW_COUNT,
    MAX_ROW_COUNT,
    DEFAULT_DATA_ROW,
    DEFAULT_DATA_SOURCE,
    getBillDetails,
};