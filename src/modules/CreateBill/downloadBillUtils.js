import React from 'react';
import ReactDOM from 'react-dom';
import BillSample from '../BillSample/BillSample';

import html2canvas from 'html2canvas';
import jsPDF from 'jspdf';

const downloadBill = (billDetails = {}) => {
    const billSampleEle = document.getElementById('bill_sample');
    ReactDOM.render(<BillSample {...billDetails}/>,billSampleEle);
    const timer = setInterval(() => {
        const ele = document.getElementById('bill');
        if(ele) {
            clearInterval(timer);
            html2canvas(ele)
                .then((canvas) => {
                    const imgData = canvas.toDataURL('image/png');
                    const pdf = new jsPDF();
                    pdf.addImage(imgData, 'PNG', 0, 0);
                    pdf.save("bill.pdf");  
                });  
        }      
    },200);
};

export default {
    downloadBill,
};
