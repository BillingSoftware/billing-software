import React from 'react';
import styles from './ManageBills.module.css';

// Actions
import { connect } from "react-redux";
import actions from '../../reduxStore/actions'; 

// Components
import { Typography, Table } from 'antd';

// Utils
import manageBillUtils from './manageBillsUtils';

const ManageBills = props => {
  const { billsList, manageBillsColumnConfig, deleteBill } = props;
  return (
    <div className={styles.section_container}>
      <Typography.Title>Manage Bills</Typography.Title>
      <Table 
        pagination={manageBillUtils.tablePaginationProperties}
        scroll={manageBillUtils.tableScrollProperties}
        columns={manageBillUtils.getFilteredTableColumns(manageBillsColumnConfig,deleteBill)} 
        dataSource={billsList} 
        bordered={true}/>
    </div>
  );
};

const mapStateToProps = state => {
  return {
    billsList: state.billsList,
    manageBillsColumnConfig: state.manageBillsColumnConfig,
  }
};

const mapDispatchToProps = dispatch => {
  return {
    deleteBill: (index) => dispatch(actions.deleteBill(index)),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(React.memo(ManageBills));

