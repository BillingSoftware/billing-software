import React from 'react';
import { Space } from 'antd';
import { noop as _noop } from 'lodash';

// Utils
import downloadBillUtils from '../CreateBill/downloadBillUtils';

const tableScrollProperties = {x:'100vw'};
const tablePaginationProperties = { pageSize: 25 };

const getTableColumns = (onDelete = _noop) => {
  return [
    {
      title: 'Customer Name',
      dataIndex: 'name',
      isRequired: true,
      render: (_,record) => (<p>{record?.customerDetails?.name}</p>),
    },
    {
      title: 'Bill Number',
      dataIndex: 'billNumber',
      render: (_,record) => (<p>{record?.billNumber}</p>),
    },
    {
      title: 'Bill Date',
      dataIndex: 'billNumberDate',
      render: (_,record) => (<p>{record?.billNumberDate}</p>),
    },
    {
      title: 'Challan Number',
      dataIndex: 'challanNumber',
      render: (_,record) => (<p>{record?.challanNumber}</p>),
    },
    {
      title: 'Challan Date',
      dataIndex: 'challanNumberDate',
      render: (_,record) => (<p>{record?.challanNumberDate}</p>),
    },
    {
      title: 'Order Number',
      dataIndex: 'orderNumber',
      render: (_,record) => (<p>{record?.orderNumber}</p>),
    },
    {
      title: 'Order Date',
      dataIndex: 'orderNumberDate',
      render: (_,record) => (<p>{record?.orderNumberDate}</p>),
    },
    {
      title: 'Total Amount\n(Without Tax)',
      dataIndex: 'totalAmountWithoutTax',
      render: (_,record) => (<p>{record?.priceDetails?.totalAmountWithoutTax}</p>),
    },
    {
      title: 'SGST',
      dataIndex: 'SGST',
      render: (_,record) => (<p>{record?.priceDetails?.SGST}</p>),
    },
    {
      title: 'CGST',
      dataIndex: 'CGST',
      render: (_,record) => (<p>{record?.priceDetails?.CGST}</p>),
    },
    {
      title: 'IGST',
      dataIndex: 'IGST',
      render: (_,record) => (<p>{record?.priceDetails?.IGST}</p>),
    },
    {
      title: 'Total Amount\n(With Tax)',
      dataIndex: 'totalAmountWithTax',
      render: (_,record) => (<p>{record?.priceDetails?.totalAmountWithTax}</p>),
    },
    {
      title: 'Actions',
      dataIndex: 'actions',
      isRequired: true,
      render: (_,record,index) => {
        return (
          <Space>
            <a onClick={() => onDelete(index)}>Delete</a>
            <a onClick={() => downloadBillUtils.downloadBill(record)}>Download</a>
          </Space>
        );
      },
    },
  ];
};

const getFilteredTableColumns = (columnConfig,onDelete) => {
  return getTableColumns(onDelete).filter(column => columnConfig[column.dataIndex] || column.isRequired);
};

export default {
  getTableColumns,
  tableScrollProperties,
  tablePaginationProperties,
  getFilteredTableColumns,
};