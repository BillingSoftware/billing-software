import React from 'react';
import numberToWords from 'number-to-words';
import { times as _times, constant as _constant, isNumber as _isNumber } from 'lodash';

// Components
import { Typography, Row, Col } from 'antd';
const { Text } = Typography;

const WIDTH = '790px';
const HEIGHT = '1120px';

const __ = (text) => (text || text===0 ? text : <>&nbsp;</>);

const getContactInfo = ({ contactNo,email } = {}) => {
    const contactNoString = contactNo ? `Phone No: ${contactNo}` : '';
    const emailString = email ? `Email: ${email}` : '';

    if(!!contactNoString && !!emailString) {
        return `${contactNoString} | ${emailString}`;
    }
    return `${contactNoString}${emailString}`;
};

const getColumnsData = (selledProducts = []) => {
    const updatedSelledProducts = selledProducts.map((column,index) => ({...(column || {}),position: index+1}))
    const columnCount = updatedSelledProducts.length;
    return [...updatedSelledProducts,...( _times(20-columnCount,_constant({})) )];
};

const getNumberToWords = (amount) => _isNumber(amount) ? `${numberToWords.toWords(amount)} only` : amount;

const BillSample = (props) => {
    
    const { companyDetails, customerDetails, taxDetails, priceDetails, selledProducts } = props;

    return (
    <div id="bill" style={{ display:'flex', justifyContent:'center', alignItems:'center', width:WIDTH, minWidth:WIDTH, maxWidth:WIDTH, height:HEIGHT, minHeight:HEIGHT, maxHeight:HEIGHT }}>
        <div style={{ backgroundColor: 'white', textAlign:'center', border: '1px solid black', width:'100%' }}>
            
            {/* Company Details */}
            <div style={{ border: '1px solid black' }}>
                <Row>
                    <Col span={12}>
                        <Text strong style={{height: '100%', fontSize: '20px', display: 'flex', justifyContent: 'center', alignItems: 'center'}}>{__('ORIGINAL / DUPLICATE')}</Text>
                    </Col>
                    <Col span={12}>
                        <Text strong style={{fontSize: "50px"}}>{__(companyDetails?.name)}</Text>
                    </Col>
                </Row>

                <Row>
                    <Col span={12}>
                        {__('')}
                    </Col>
                    <Col span={12} style={{textAlign: "left"}}>
                        <Text strong>{__(companyDetails?.address1)}</Text>
                    </Col>
                </Row>

                <Row>
                    <Col span={12}>
                        <Text strong>{__(companyDetails?.gstNo ? `GSTIN : ${companyDetails?.gstNo}` : '')}</Text>
                    </Col>
                    <Col span={12} style={{textAlign: "left"}}>
                        <Text strong>{__(companyDetails?.address2)}</Text>
                    </Col>
                </Row>

                <Row>
                    <Col span={12}>
                        {__('')}
                    </Col>
                    <Col span={12} style={{textAlign: "left"}}>
                        <Text strong>{__(getContactInfo(companyDetails))}</Text>
                    </Col>
                </Row>
            </div>

            {/* Customer Details + Bill Details */}
            <Row>

                {/* Customer Details */}
                <Col span={12} style={{ border: '1px solid black' }}>
                    <Row>
                        <Col span={4}>
                            <Text strong>{__('M/s')}</Text>
                        </Col>
                        <Col span={20} style={{textAlign: "left"}}>
                            <Text strong>{__(customerDetails?.name)}</Text>
                        </Col>
                    </Row>
                    <Row>
                        <Col span={4}>
                            <Text>{__('')}</Text>
                        </Col>
                        <Col span={20} style={{textAlign: "left"}}>
                            <Text strong>{__(customerDetails?.address1)}</Text>
                        </Col>
                    </Row>
                    <Row>
                        <Col span={4}>
                            <Text>{__('')}</Text>
                        </Col>
                        <Col span={20} style={{textAlign: "left"}}>
                            <Text strong>{__(customerDetails?.address2)}</Text>
                        </Col>
                    </Row>
                    <Row>
                        <Col span={24}>
                            {__('')}
                        </Col>
                    </Row>
                    <Row>
                        <Col span={24}>
                            <Text strong>{__(customerDetails?.gstNo ? `Customer GST : ${customerDetails?.gstNo}` : '')}</Text>
                        </Col>
                    </Row>
                </Col>

                {/* Bill Details */}
                <Col span={12} style={{ border: '1px solid black' }}>
                    <Row>
                        <Col span={24}>
                            <Text strong>{__('GST TAX INVOICE')}</Text>
                        </Col>
                    </Row>
                    <Row>
                        <Col span={24}>
                            {__('')}
                        </Col>
                    </Row>
                    <Row>
                        <Col span={6} style={{textAlign: "left", paddingLeft: "12px"}}>
                            <Text strong>{__('Bill No.')}</Text>
                        </Col>
                        <Col span={6} style={{textAlign: "left"}}>
                            <Text>{__(props.billNumber)}</Text>
                        </Col>
                        <Col span={6} style={{textAlign: "left"}}>
                            <Text strong>{__('Date')}</Text>
                        </Col>
                        <Col span={6} style={{textAlign: "left"}}>
                            <Text>{__(props.billNumberDate)}</Text>
                        </Col>
                    </Row>
                    <Row>
                        <Col span={6} style={{textAlign: "left", paddingLeft: "12px"}}>
                            <Text strong>{__('Challan No.')}</Text>
                        </Col>
                        <Col span={6} style={{textAlign: "left"}}>
                            <Text>{__(props.challanNumber)}</Text>
                        </Col>
                        <Col span={6} style={{textAlign: "left"}}>
                            <Text strong>{__('Date')}</Text>
                        </Col>
                        <Col span={6} style={{textAlign: "left"}}>
                            <Text>{__(props.challanNumberDate)}</Text>
                        </Col>
                    </Row>
                    <Row>
                        <Col span={6} style={{textAlign: "left", paddingLeft: "12px"}}>
                            <Text strong>{__('Order No.')}</Text>
                        </Col>
                        <Col span={6} style={{textAlign: "left"}}>
                            <Text>{__(props.orderNumber)}</Text>
                        </Col>
                        <Col span={6} style={{textAlign: "left"}}>
                            <Text strong>{__('Date')}</Text>
                        </Col>
                        <Col span={6} style={{textAlign: "left"}}>
                            <Text>{__(props.orderNumberDate)}</Text>
                        </Col>
                    </Row>
                </Col>
            </Row>

            {/* Table Container */}
            <div>
                <Row>
                    <Col span={2} style={{ border: '1px solid black', borderBottomWidth: '2px' }}>
                        <Text strong>{__('Sr. No.')}</Text>
                    </Col>
                    <Col span={10} style={{ border: '1px solid black', borderBottomWidth: '2px' }}>
                        <Text strong>{__('Description')}</Text>
                    </Col>
                    <Col span={3} style={{ border: '1px solid black', borderBottomWidth: '2px' }}>
                        <Text strong>{__('Code')}</Text>
                    </Col>
                    <Col span={3} style={{ border: '1px solid black', borderBottomWidth: '2px' }}>
                        <Text strong>{__('Rate (Rs.)')}</Text>
                    </Col>
                    <Col span={3} style={{ border: '1px solid black', borderBottomWidth: '2px' }}>
                        <Text strong>{__('Quantity')}</Text>
                    </Col>
                    <Col span={3} style={{ border: '1px solid black', borderBottomWidth: '2px' }}>
                        <Text strong>{__('Amount (Rs.)')}</Text>
                    </Col>
                </Row>
                {
                    getColumnsData(selledProducts).map((column) => {
                        return (
                            <Row>
                                <Col span={2} style={{ border: '1px solid black', borderTop:'none', borderBottom:'' }}>
                                    <Text strong>{__(column?.position)}</Text>
                                </Col>
                                <Col span={10} style={{ border: '1px solid black', borderTop:'none', borderBottom:'', textAlign:'left' }}>
                                    <Text strong>{__(column?.product)}</Text>
                                </Col>
                                <Col span={3} style={{ border: '1px solid black', borderTop:'none', borderBottom:'' }}>
                                    <Text strong>{__(column?.code)}</Text>
                                </Col>
                                <Col span={3} style={{ border: '1px solid black', borderTop:'none', borderBottom:'' }}>
                                    <Text strong>{__(column?.rate)}</Text>
                                </Col>
                                <Col span={3} style={{ border: '1px solid black', borderTop:'none', borderBottom:'' }}>
                                    <Text strong>{__(column?.quantity)}</Text>
                                </Col>
                                <Col span={3} style={{ border: '1px solid black', borderTop:'none', borderBottom:'' }}>
                                    <Text strong>{__(column?.amount)}</Text>
                                </Col>
                            </Row>
                        );
                    })
                }
                {
                    [
                        { label: 'Total Rs.', value: priceDetails?.totalAmountWithoutTax },
                        { label: `SGST ${taxDetails?.SGST || 0}%`, value: priceDetails?.SGST },
                        { label:`CGST ${taxDetails?.CGST || 0}%`, value: priceDetails?.CGST },
                        { label: `IGST ${taxDetails?.IGST || 0}%`, value: priceDetails?.IGST },
                    ].map(({ label, value },index) => {
                        return (
                            <Row>
                                <Col span={2} style={{ border: '1px solid black', borderTop:'none', borderBottomWidth: index===3 ? '1px' : '0' }}>
                                    <Text strong>{__('')}</Text>
                                </Col>
                                <Col span={10} style={{ border: '1px solid black', borderTop:'none', borderBottomWidth: index===3 ? '1px' : '0' }}>
                                    <Text strong>{__('')}</Text>
                                </Col>
                                <Col span={3} style={{ border: '1px solid black', borderTop:'none', borderBottomWidth: index===3 ? '1px' : '0' }}>
                                    <Text strong>{__('')}</Text>
                                </Col>
                                <Col span={3} style={{ border: '1px solid black', borderTop:'none', borderBottomWidth: index===3 ? '1px' : '0' }}>
                                    <Text strong>{__('')}</Text>
                                </Col>
                                <Col span={3} style={{ border: '1px solid black', borderTopWidth: index===0 ? '2px' : '1px' }}>
                                    <Text strong>{__(label)}</Text>
                                </Col>
                                <Col span={3} style={{ border: '1px solid black', borderTopWidth: index===0 ? '2px' : '1px' }}>
                                    <Text strong>{__(value)}</Text>
                                </Col>
                            </Row>
                        );
                    })
                }
            </div>

            {/* Proce Details Container */}
            <Row>
                <Col span={2} style={{ border: '1px solid black' }}>
                    <Text strong>{__('Rs.')}</Text>
                </Col>
                <Col span={16} style={{ border: '1px solid black', textAlign: 'left', paddingLeft: '12px' }}>
                    <Text strong>{__(getNumberToWords(priceDetails?.totalAmountWithTax))}</Text>
                </Col>
                <Col span={3} style={{ border: '1px solid black' }}>
                    <Text strong>{__('Total')}</Text>
                </Col>
                <Col span={3} style={{ border: '1px solid black' }}>
                    <Text strong>{__(priceDetails?.totalAmountWithTax)}</Text>
                </Col>
            </Row>

            {/* Terms And Conditions Container */}
            <Row>
                <Col span={18} style={{ border: '1px solid black', whiteSpace: 'pre-line', textAlign:'left', paddingLeft: '12px', minHeight: '100px', display: 'flex', alignItems: 'center' }}>
                    <Text strong>{__(companyDetails?.termsAndConditions ? `Terms And Conditions : \n${companyDetails?.termsAndConditions}` : '')}</Text>
                </Col>
                <Col span={6} style={{ border: '1px solid black', display: 'flex', justifyContent: 'center', alignItems: 'flex-end', fontSize: '20px' }}>
                    <Text strong>{__('Authorized Sig.')}</Text>
                </Col>
            </Row>

        </div>
    </div>
  );
};

export default React.memo(BillSample);
