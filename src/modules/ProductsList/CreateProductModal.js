import React, { useState, useCallback } from 'react';

// Components
import { Form, Modal } from 'antd';
import Input from '../../components/Input/Input';
import InputNumber from '../../components/InputNumber/InputNumber';

const CreateProductModal = (props) => {
    const { value, mode, onOk } = props;
    
    const [productDetails, setProductDetails] = useState(value || {
        name: "",
        code: "",
        cost: "",
    });

    const onProductDetailsChange = useCallback((changeItem) => {
        setProductDetails({
            ...productDetails,
            ...changeItem,
        })
    },[productDetails,setProductDetails]);

    const onCreateProduct = useCallback(() => {
        if(!onOk) {
            return;
        }
        onOk(productDetails);
    },[productDetails,onOk]);

    return (
        <Modal 
            {...props}
            onOk={onCreateProduct}>
            <Form 
                labelCol={{ span: 6 }}
                wrapperCol={{span: 16}}>
                <Form.Item label="Name">
                    <Input
                        datakey="name"
                        placeholder="Please Enter Product Name"
                        disabled={mode==='EDIT'}
                        value={productDetails}
                        onChange={onProductDetailsChange}/>
                </Form.Item>
                <Form.Item label="Code">
                    <Input
                        datakey="code"
                        placeholder="Please Enter Product Code"
                        value={productDetails}
                        onChange={onProductDetailsChange}/>
                </Form.Item>
                <Form.Item label="Cost">
                    <InputNumber
                        style={{width: "100%"}}
                        datakey="cost"
                        placeholder="Please Enter Product Cost"
                        value={productDetails}
                        onChange={onProductDetailsChange}/>
                </Form.Item>
            </Form>
        </Modal>
    );
};

export default React.memo(CreateProductModal);