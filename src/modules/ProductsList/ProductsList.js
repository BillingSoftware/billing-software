import React, { useState, useCallback, useMemo } from 'react';
import styles from './ProductsList.module.css';
import { find as _find } from 'lodash';

// Actions
import { connect } from "react-redux";
import actions from '../../reduxStore/actions'; 

// Components
import { Typography, Table, Button, Space, Modal } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import CreateProductModal from './CreateProductModal';

const getTableColumns = (onEditExistingProductClick,deleteProduct) => {
  return [
    {
      title: 'Name',
      dataIndex: 'name',
    },
    {
      title: 'Code',
      dataIndex: 'code',
    },
    {
      title: 'Cost',
      dataIndex: 'cost',
    }, 
    {
      title: 'Actions',
      dataIndex: 'actions',
      render: (text,record) => {
        return (
          <Space>
            <a onClick={() => onEditExistingProductClick(record)}>Edit</a>
            <a onClick={() => deleteProduct(record)}>Delete</a>
          </Space>
        );
      },
    }
  ];  
};

const ProductsList = props => {
  const { 
    productsList, 
    createProduct, 
    editProduct, 
    deleteProduct, 
  } = props;

  const [showCreateProductModal, setShowCreateProductModal] = useState({
    visible: false,
    mode: undefined,
    value: undefined,
  });

  // Open and close modal

  const openCreateProductModal = useCallback((mode,value) => {
    setShowCreateProductModal({
      visible: true,
      mode,
      value,
    });
  },[setShowCreateProductModal]);

  const closeCreateProductModal = useCallback(() => {
    setShowCreateProductModal({
      visible: false,
      mode: undefined,
      value: undefined,
    });
  },[setShowCreateProductModal]);

  // Create product

  const onCreateNewProductClick = useCallback(() => {
    openCreateProductModal('CREATE');
  },[openCreateProductModal]);

  const createNewProduct = useCallback((productDetails) => {
    const existingProduct = _find(productsList,['name',productDetails.name]);
    if(existingProduct) {
      Modal.error({ title: 'Product with same name already exist, please use different name.' });
      return;
    }
    
    if(createProduct) {
      createProduct(productDetails);
    }
    closeCreateProductModal();
  },[productsList,createProduct,closeCreateProductModal]);

  // Edit product

  const onEditExistingProductClick = useCallback((productDetails) => {
    openCreateProductModal('EDIT',productDetails);
  },[openCreateProductModal]);

  const editExistingProduct = useCallback((productDetails) => {
    if(editProduct) {
      editProduct(productDetails);
    }
    closeCreateProductModal();
  },[editProduct,closeCreateProductModal]);

  const tableColumns = useMemo(() => getTableColumns(onEditExistingProductClick,deleteProduct),[onEditExistingProductClick,deleteProduct]);

  return (
    <div className={styles.section_container}>
      <Typography.Title>Products List</Typography.Title>
      <Button
        type="dashed"
        onClick={onCreateNewProductClick}
        style={{marginBottom: "24px", width: "100%"}}
      > 
        <PlusOutlined /> Create New Product
      </Button>
      <Table 
        rowKey="name"
        columns={tableColumns} 
        dataSource={productsList} 
        bordered={true}/>
      {showCreateProductModal.visible && (
        <CreateProductModal
          {...showCreateProductModal}
          title="Enter Details Of New Product"
          centered={true}
          onOk={showCreateProductModal.mode==='CREATE' ? createNewProduct : editExistingProduct}
          onCancel={closeCreateProductModal} />
      )}
    </div>
  );
};

const mapStateToProps = state => {
  return {
    productsList: state.productsList
  }
};

const mapDispatchToProps = dispatch => {
  return {
    createProduct: (productDetails) => dispatch(actions.createProduct(productDetails)),
    editProduct: (productDetails) => dispatch(actions.editProduct(productDetails)),
    deleteProduct: (productDetails) => dispatch(actions.deleteProduct(productDetails)),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(React.memo(ProductsList));

