import React, { useCallback } from 'react';
import PropTypes from 'prop-types';

import { Input as InputField } from 'antd';

const TextArea = React.memo(props => {
    const { datakey, onChange, rows } = props;

    const onInputChange = useCallback((event) => {
        if(!onChange) {
            return;
        }
        const newValue = event.target.value;
        if(datakey) {
            onChange({ [datakey]: newValue});
        } else {
            onChange(newValue);
        }
    },[onChange,datakey]);

    const value = datakey ? props.value?.[datakey] : props.value;
    
    return (
        <InputField.TextArea 
            {...props}
            rows={rows}
            value={value}
            onChange={onInputChange}/>
    );
});

TextArea.propTypes = {
    rows: PropTypes.number,
    onChange: PropTypes.func,
    datakey: PropTypes.string,
    value: PropTypes.oneOfType([PropTypes.string,PropTypes.object]),
};

TextArea.defaultProps = {
    rows: 4,
}

export default TextArea;