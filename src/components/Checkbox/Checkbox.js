import React, { useCallback } from 'react';
import PropTypes from 'prop-types';

import { Checkbox as CheckboxField } from 'antd';

const Checkbox = React.memo(props => {
    const { datakey, onChange } = props;

    const onCheckboxChange = useCallback((event) => {
        if(!onChange) {
            return;
        }
        const newValue = event.target.checked;
        if(datakey) {
            onChange({ [datakey]: newValue});
        } else {
            onChange(newValue);
        }
    },[onChange,datakey]);

    const value = datakey ? props.value?.[datakey] : props.value;
    
    return (
        <CheckboxField 
            {...props}
            checked={value}
            onChange={onCheckboxChange}/>
    );
});

Checkbox.propTypes = {
    onChange: PropTypes.func,
    datakey: PropTypes.string,
    value: PropTypes.oneOfType([PropTypes.string,PropTypes.object]),
};

export default Checkbox;