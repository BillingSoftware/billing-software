import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import { round as _round, isNumber as _isNumber } from 'lodash';

import { InputNumber as InputNumberField } from 'antd';

const InputNumber = React.memo(props => {
    const { datakey, onChange, precision } = props;

    const onInputNumberChange = useCallback((newValue) => {
        if(!onChange || !_isNumber(newValue)) {
            return;
        }
        if(precision) {
            newValue = _round(newValue,precision);
        }
        if(datakey) {
            onChange({ [datakey]: newValue});
        } else {
            onChange(newValue);
        }
    },[onChange,datakey,precision]);

    const valueWithoutPrecision = datakey ? props.value?.[datakey] : props.value;
    const value = valueWithoutPrecision ? 
        (precision ? _round(valueWithoutPrecision,precision) : 
        valueWithoutPrecision) : valueWithoutPrecision;

    return (
        <InputNumberField 
            {...props}
            value={value}
            onChange={onInputNumberChange}/>
    );
});

InputNumber.propTypes = {
    onChange: PropTypes.func,
    datakey: PropTypes.string,
    precision: PropTypes.number,
    value: PropTypes.oneOfType([PropTypes.string,PropTypes.object]),
};

export default InputNumber;