import React, { useCallback } from 'react';
import PropTypes from 'prop-types';

import { Input as InputField } from 'antd';

const Input = React.memo(props => {
    const { datakey, onChange } = props;

    const onInputChange = useCallback((event) => {
        if(!onChange) {
            return;
        }
        const newValue = event.target.value;
        if(datakey) {
            onChange({ [datakey]: newValue});
        } else {
            onChange(newValue);
        }
    },[onChange,datakey]);

    const value = datakey ? props.value?.[datakey] : props.value;
    
    return (
        <InputField 
            {...props}
            value={value}
            onChange={onInputChange}/>
    );
});

Input.propTypes = {
    onChange: PropTypes.func,
    datakey: PropTypes.string,
    value: PropTypes.oneOfType([PropTypes.string,PropTypes.object]),
};

export default Input;