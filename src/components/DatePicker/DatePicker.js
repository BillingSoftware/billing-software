import React, { useCallback } from 'react';
import PropTypes from 'prop-types';

import { DatePicker as DatePickerField } from 'antd';

const DatePicker = React.memo(props => {
    const { datakey, onChange } = props;

    const onDatePickerChange = useCallback((date,dateString) => {
        if(!onChange) {
            return;
        }
        const newValue = dateString;
        if(datakey) {
            onChange({ [datakey]: newValue});
        } else {
            onChange(newValue);
        }
    },[onChange,datakey]);

    const value = datakey ? props.value?.[datakey] : props.value;
    
    return (
        <DatePickerField 
            {...props}
            value={value}
            onChange={onDatePickerChange}/>
    );
});

DatePicker.propTypes = {
    onChange: PropTypes.func,
    datakey: PropTypes.string,
    value: PropTypes.oneOfType([PropTypes.string,PropTypes.object]),
};

export default DatePicker;