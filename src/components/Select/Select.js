import React, { useCallback } from 'react';
import PropTypes from 'prop-types';

import { Select as SelectField } from 'antd';

const Select = React.memo(props => {
    const { options, datakey, labelkey, valuekey, onChange } = props;

    const onSelectChange = useCallback((selectedValue,selectedOption) => {
        if(!onChange) {
            return;
        }
        if(datakey) {
            onChange({ [datakey]: selectedValue},selectedOption);
        } else {
            onChange(selectedValue,selectedOption);
        }
    },[onChange,datakey]);

    const getOptions = useCallback((options) => {
        return options.map((option) => {
            return {
                ...option,
                label: option[labelkey],
                value: option[valuekey],
            };
        });
    },[labelkey,valuekey])

    const value = datakey ? props.value?.[datakey] : props.value;

    return (
        <SelectField 
            {...props}
            value={value}
            options={getOptions(options)}
            onChange={onSelectChange}/>
    );
});

Select.propTypes = {
    onChange: PropTypes.func,
    datakey: PropTypes.string,
    value: PropTypes.oneOfType([PropTypes.string,PropTypes.object]),
};

export default Select;